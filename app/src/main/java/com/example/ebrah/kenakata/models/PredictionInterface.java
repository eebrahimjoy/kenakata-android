package com.example.ebrah.kenakata.models;


import com.example.ebrah.kenakata.models.PlaceAPI.Prediction;

public interface PredictionInterface {
    void getPrediction(Prediction prediction);
}
