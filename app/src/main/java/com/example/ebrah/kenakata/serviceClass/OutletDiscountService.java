package com.example.ebrah.kenakata.serviceClass;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ebrah.kenakata.R;
import com.example.ebrah.kenakata.models.Outlet;
import com.example.ebrah.kenakata.models.retrofit.ApiService;
import com.example.ebrah.kenakata.models.retrofit.RetrofitInstance;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;


import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OutletDiscountService extends Service {
    private static final int LOCATION_INTERVAL = 2000;
    private static final int FAST_LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 1f;
    private FusedLocationProviderClient client;
    private LocationRequest request;
    private LocationCallback callback;
    private ApiService outletService;
    private String url;
    private String shopN;
    private  String shopA;

    int maxDiscount = 0;


    public OutletDiscountService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {


        return START_STICKY;
    }

    private void processLocation(double latitude, double longitude) {
        getApiData(latitude, longitude);
    }

    private void stopThisService()
    {
        Toast.makeText(getApplicationContext(), "Stopping Service", Toast.LENGTH_SHORT).show();
        stopSelf();
    }

    public static boolean isConnectedToNetwork(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isConnected = false;
        if (connectivityManager != null) {
            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
            isConnected = (activeNetwork != null) && (activeNetwork.isConnectedOrConnecting());
        }

        return isConnected;
    }

    private static Timer timer;
    private Context ctx;

    private void startService()
    {
        timer = new Timer();
        timer.scheduleAtFixedRate(new mainTask(), 1000, 2000);
    }

    private class mainTask extends TimerTask
    {
        int count = 0;
        public void run()
        {
            if (count == 3)
            {
                timer.cancel();
                toastHandler2.sendEmptyMessage(0);
                return;
            }
            if (!isConnectedToNetwork(ctx)) {
                count++;
            } else {
                timer.cancel();
                toastHandler.sendEmptyMessage(0);
            }
        }
    }

    private final Handler toastHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            ((OutletDiscountService)ctx).doJob();
        }
    };

    private final Handler toastHandler2 = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            ((OutletDiscountService)ctx).stopThisService();
        }
    };

    public void doJob()
    {
        final LocationManager manager = (LocationManager)this.getSystemService    (Context.LOCATION_SERVICE );

        if ( manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            client = LocationServices.getFusedLocationProviderClient(this);
            request = new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(LOCATION_INTERVAL).setFastestInterval(FAST_LOCATION_INTERVAL);


            callback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);

                    if (locationResult == null || locationResult.getLocations().isEmpty())
                    {
                        stopThisService();
                        return;
                    }

                    for (Location location : locationResult.getLocations()) {
                        processLocation(location.getLatitude(), location.getLongitude());
                    }
                }
            };

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                stopThisService();
                return;
            }


            client.requestLocationUpdates(request, callback, null);
        }
        else {
            Toast.makeText(this, "failed", Toast.LENGTH_SHORT).show();
            url=String.format("FindDiscounts/Discounts");
            outletService=RetrofitInstance.getRetrofitInstanceForOutletAPI().create(ApiService.class);
            Call<List<Outlet>> call = outletService.getOutlets(url);
            call.enqueue(new Callback<List<Outlet>>() {
                @Override
                public void onResponse(Call<List<Outlet>> call, Response<List<Outlet>> response) {

                    final List<Outlet> outlets = response.body();

                    for (int i = 0; i < outlets.size(); i++) {
                        maxDiscount = outlets.get(0).getDiscountAmount();
                        shopN=outlets.get(0).getShopName();
                        shopA=outlets.get(0).getFullAddress();
                    }
                    ShowNotification(maxDiscount,shopN,shopA);
                    stopThisService();
                }

                @Override
                public void onFailure(Call<List<Outlet>> call, Throwable t) {
                    Toast.makeText(OutletDiscountService.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    stopThisService();
                }
            });


        }

    }

    @Override
    public void onCreate() {
        super.onCreate();
        ctx = this;
        startService();
    }


    public void onDestroy() {
        //client.removeLocationUpdates(callback);
    }

    private void getApiData(final double latitudeValue, final double longitudeValue) {
        url = String.format("FindDiscounts/Discounts?latitude=%f&longitude=%f", latitudeValue, longitudeValue);
        outletService = RetrofitInstance.getRetrofitInstanceForOutletAPI().create(ApiService.class);
        Call<List<Outlet>> call = outletService.getOutlets(url);

        call.enqueue(new Callback<List<Outlet>>() {


            public void onFailure(Call<List<Outlet>> call, Throwable t) {
                stopThisService();
            }



            @Override
            public void onResponse(Call<List<Outlet>> call, Response<List<Outlet>> response) {
                if (!response.isSuccessful()) {
                    stopThisService();
                    return;
                }

                final List<Outlet> outlets = response.body();

                if (outlets.size() == 0) {
                    stopThisService();
                    return;
                }




                for (int i = 0; i < outlets.size(); i++) {
                    final int discount = outlets.get(i).getDiscountAmount();
                    shopN=outlets.get(i).getShopName();
                     shopA=outlets.get(i).getFullAddress();
                    if (discount > maxDiscount)
                    {
                        maxDiscount = discount;
                    }
                }

                ShowNotification(maxDiscount,shopN,shopA);
                stopThisService();
            }
        });
    }

    private void ShowNotification(Integer content,String txt,String txt1) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, "notification_id")
                        .setSmallIcon(R.drawable.ic_launcher_background).setDefaults(Notification.DEFAULT_SOUND);;

        RemoteViews notificationContent = new RemoteViews(getPackageName(),
                R.layout.notification);



        notificationContent.setTextViewText(R.id.shopName, shopN);
        notificationContent.setTextViewText(R.id.shopAddress,shopA);
        notificationContent.setTextViewText(R.id.discountAmount,String.valueOf(maxDiscount));


        mBuilder.setContent(notificationContent);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(0, mBuilder.build());
    }
}