package com.example.ebrah.kenakata.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.example.ebrah.kenakata.R;
import com.example.ebrah.kenakata.models.Outlet;
import com.example.ebrah.kenakata.models.retrofit.ApiService;
import com.example.ebrah.kenakata.models.retrofit.RetrofitInstance;
import com.example.ebrah.kenakata.serviceClass.OutletDiscountService;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, RoutingListener {
    private static final int[] COLORS = new int[]{R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark};
    private List<Polyline> polylines;
    private ApiService outletService;
    private String url;
    private double latFromApi;
    private double lngFromApi;
    Animation layoutAnimationOut;
    private static final int _START_ACTIVITY_FOR_RESULT_REQUEST_CODE = 1;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private ImageButton backButtonMain;
    private Button placeName;
    private LinearLayout linearLayoutBottomPart;
    private FloatingActionButton currentLocation;
    private TextView searchText, greetingMessage,snackBar;
    private Snackbar snackbar1;
    private double currentLat;
    private double currentLon;
    private LatLng myLocation;
    private GoogleMap map;
    private GoogleMapOptions options;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private boolean doubleBackToExitPressedOnce = false;
    private int flagForVisibilityCheck = 0;

    /*private ClusterManager<MyItem> clusterManager;*/
    @Override
    public void onBackPressed() {
        if (placeName.getVisibility() == View.VISIBLE && flagForVisibilityCheck == 0) {
            Intent mainIntent = new Intent(MainActivity.this, MainActivity.class);
            flagForVisibilityCheck = 1;
            startActivity(mainIntent);
        } else if (doubleBackToExitPressedOnce == true) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            System.exit(0);
        }
        {
            doubleBackToExitPressedOnce = true;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;

                }
            }, 2000);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == _START_ACTIVITY_FOR_RESULT_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                double resultLat = data.getDoubleExtra("latitude", 0);
                double resultLon = data.getDoubleExtra("longitude", 0);
                LatLng searchLatLng = new LatLng(resultLat, resultLon);
                map.clear();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(searchLatLng, 12);
                map.animateCamera(cameraUpdate);
                String userSearchAddress = data.getStringExtra("lineAddress");
                placeName.setVisibility(View.VISIBLE);
                backButtonMain.setVisibility(View.VISIBLE);
                linearLayoutBottomPart.setAnimation(layoutAnimationOut);
                linearLayoutBottomPart.setVisibility(View.GONE);
                currentLocation.setVisibility(View.GONE);
                placeName.setText(userSearchAddress);

                getApiData(resultLat, resultLon);
                backButtonMain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent mainIntent = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(mainIntent);
                    }
                });
            }
            if (resultCode == Activity.RESULT_CANCELED) {
               // Toast.makeText(this, "You aren't enter any location", Toast.LENGTH_SHORT).show();
            }
        }


    }


    public static Bitmap createCustomMarker(Context context, @DrawableRes int resource, int discount) {

        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);

        ImageView markerImage = marker.findViewById(R.id.user_dp);
        markerImage.setImageResource(resource);
        TextView discountAmount = marker.findViewById(R.id.discountTV);
        discountAmount.setText(String.valueOf(discount));
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);

        return bitmap;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Start the service
        Intent serviceIntent = new Intent(getApplicationContext(), OutletDiscountService.class);
        startService(serviceIntent);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        polylines = new ArrayList<>();

        placeName = findViewById(R.id.searchedBTN);
        searchText = findViewById(R.id.inputSearchId);
        currentLocation = findViewById(R.id.currentLocationFAB);
        greetingMessage = findViewById(R.id.greetingMessageTV);
        linearLayoutBottomPart = findViewById(R.id.linearLayoutId);
        backButtonMain = findViewById(R.id.backButtonMainIB);
        snackBar = findViewById(R.id.snackbarTV);
        getMapReady();
        setGreetingMessage();

        currentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserLastLocation();

            }
        });
        searchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivityForResult(intent, _START_ACTIVITY_FOR_RESULT_REQUEST_CODE);
            }
        });
        placeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivityForResult(intent, _START_ACTIVITY_FOR_RESULT_REQUEST_CODE);

            }
        });
        layoutAnimationOut = AnimationUtils.loadAnimation(this, R.anim.exit_to_bottom);
    }

    public void setGreetingMessage() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        String greeting = null;
        if (hour >= 5 && hour < 12) {
            greeting = "Good Morning";
            greetingMessage.setText(greeting);
        } else if (hour >= 12 && hour < 16) {
            greeting = "Good Afternoon";
            greetingMessage.setText(greeting);
        } else if (hour >= 16 && hour <= 23) {
            greeting = "Good Evening";
            greetingMessage.setText(greeting);


        } else {
            greeting = "Good Night";
            greetingMessage.setText(greeting);


        }
    }

    public static void expand(final View linearLayoutBottomPart) {
        linearLayoutBottomPart.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = linearLayoutBottomPart.getMeasuredHeight();
        linearLayoutBottomPart.getLayoutParams().height = 1;
        linearLayoutBottomPart.setVisibility(View.VISIBLE);
        Animation animationForEnter = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                linearLayoutBottomPart.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                linearLayoutBottomPart.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animationForEnter.setDuration(1800);
        linearLayoutBottomPart.startAnimation(animationForEnter);
    }

    public void getApiData(final double latitudeValue, final double longitudeValue) {
        url = String.format("FindDiscounts/Discounts?latitude=%f&longitude=%f", latitudeValue, longitudeValue);
        outletService = RetrofitInstance.getRetrofitInstanceForOutletAPI().create(ApiService.class);
        Call<List<Outlet>> call = outletService.getOutlets(url);
        call.enqueue(new Callback<List<Outlet>>() {
            @Override
            public void onResponse(Call<List<Outlet>> call, Response<List<Outlet>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Response Failed & Code Is: " + response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }
                map.clear();


                /*clusterManager.setRenderer(new MarkerClusterRenderer(MainActivity.this,map,clusterManager));
                map.setOnCameraIdleListener(clusterManager);
                map.setOnMarkerClickListener(clusterManager);*/


                final List<Outlet> outlets = response.body();

                if (outlets.size() == 0) {

                    snackBar.setVisibility(View.VISIBLE);
                    snackbar1 = Snackbar.make(snackBar, "No outlet found in this area!!", Snackbar.LENGTH_INDEFINITE)
                            .setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    snackbar1.dismiss();
                                }
                            })
                            .setActionTextColor(Color.WHITE);
                    View snackView = snackbar1.getView();
                    snackView.setBackgroundColor(getResources().getColor(R.color.colorRed));
                    TextView textView = snackView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.WHITE);
                    snackbar1.show();

                }
                else if (outlets.size() > 0){
                    if (snackbar1 != null){
                        snackbar1.dismiss();
                    }

                }


                for (int i = 0; i < outlets.size(); i++) {
                    latFromApi = outlets.get(i).getLatitude();
                    lngFromApi = outlets.get(i).getLongitude();
                    final int discount = outlets.get(i).getDiscountAmount();
                    LatLng latLng = new LatLng(latFromApi, lngFromApi);





                    /*MyItem item = new MyItem(latLng);
                    clusterManager.addItem(item);
                    clusterManager.cluster();*/

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng)
                            .icon(BitmapDescriptorFactory
                                    .fromBitmap(createCustomMarker(MainActivity.this, R.drawable.sale, discount)));
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

                    map.setMinZoomPreference(10.0f);
                    map.setMaxZoomPreference(17.0f);

                    map.addMarker(markerOptions).setTag(outlets.get(i));

                }


                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        if (marker != null) {

                            Outlet outlet = (Outlet) marker.getTag();
                            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(MainActivity.this);
                            View view = getLayoutInflater().inflate(R.layout.bottom_sheet_layout_for_outlet_details, null);
                            bottomSheetDialog.setContentView(view);
                            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from((View) view.getParent());
                            bottomSheetBehavior.setPeekHeight(
                                    (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 350, getResources()
                                            .getDisplayMetrics())
                            );
                            bottomSheetDialog.show();
                            double clickedLat = outlet.getLatitude();
                            double clickedLng = outlet.getLongitude();
                            LatLng markerClickedLatLng = new LatLng(clickedLat, clickedLng);

                            route(myLocation, markerClickedLatLng);


                            long fromDateLongToDate = outlet.getFromDate();
                            long toDateLongToDate = outlet.getToDate();

                            Date fromDate = new Date(fromDateLongToDate);
                            SimpleDateFormat fromDateInDateFormat = new SimpleDateFormat("dd MMM yyy");
                            String fromDateString = fromDateInDateFormat.format(fromDate);

                            Date toDate = new Date(toDateLongToDate);
                            SimpleDateFormat toDateInDateFormat = new SimpleDateFormat("dd MMM yyy");
                            String toDateString = toDateInDateFormat.format(toDate);


                            TextView discountName = view.findViewById(R.id.discountAmountTV);
                            discountName.setText(String.valueOf(outlet.getDiscountAmount()));
                            TextView shop = view.findViewById(R.id.shopFullNameTV);
                            shop.setText(outlet.getShopName());
                            TextView address = view.findViewById(R.id.shopAddressTV);
                            address.setText(outlet.getFullAddress());
                            TextView Date = view.findViewById(R.id.fromDateTV);
                            Date.setText(fromDateString);
                            TextView toDtae = view.findViewById(R.id.toDateTV);
                            toDtae.setText(toDateString);
                            ImageView shopImage = view.findViewById(R.id.shopImageIV);
                            Picasso.get().load(outlet.getImageURL()).into(shopImage);

                        }

                        return false;
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Outlet>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Please connect your device with internet", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void getMapReady() {
        options = new GoogleMapOptions().rotateGesturesEnabled(true).mapType(GoogleMap.MAP_TYPE_TERRAIN);
        SupportMapFragment mapFragment = SupportMapFragment.newInstance(options);
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction().replace(R.id.mapContainer, mapFragment);
        fragmentTransaction.commit();
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setCompassEnabled(false);
        changeMapStyle(map
        );
        expand(linearLayoutBottomPart);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        getUserLastLocation();

    }


    private void getUserLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission
                .ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.
                checkSelfPermission(this, Manifest.
                        permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLat = location.getLatitude();
                    currentLon = location.getLongitude();
                    myLocation = new LatLng(currentLat, currentLon);
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(myLocation, 15);
                    map.animateCamera(cameraUpdate);
                    map.getUiSettings().setMapToolbarEnabled(false);
                    map.getUiSettings().setMyLocationButtonEnabled(false);
                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    map.setMyLocationEnabled(true);
                    map.setMinZoomPreference(10.0f);
                    map.setMaxZoomPreference(17.0f);
                    getApiData(currentLat, currentLon);

                }

            }
        });

    }

    public void route(LatLng startLatLng, LatLng endLatLng) {

        if (startLatLng == null || endLatLng == null) {
            if (startLatLng == null) {
                Toast.makeText(this, "Please choose a starting point", Toast.LENGTH_SHORT).show();
            }
            if (endLatLng == null) {
                Toast.makeText(this, "Please choose a ending point", Toast.LENGTH_SHORT).show();
            }
        } else {
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.WALKING)
                    .withListener((RoutingListener) MainActivity.this)
                    .waypoints(startLatLng, endLatLng)
                    .key(MainActivity.this.getString(R.string.direction_api_key))
                    .build();
            routing.execute();
        }
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        Toast.makeText(this, "Error" + e.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
            polylines.clear();
        }
        for (int j = 0; j < route.size(); j++) {
            int colorIndex = j % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(5 + j * 3);
            polyOptions.addAll(route.get(j).getPoints());
            Polyline polyline = map.addPolyline(polyOptions);
            polylines.add(polyline);

            /*Toast.makeText(this, "DISTANCE : "+" "+Float.valueOf(route.get(shortestRouteIndex).getDistanceValue()/1000) +" KM"+ " \nDURATION :"+" "+
                    route.get(shortestRouteIndex).getDurationValue()/60+"Min", Toast.LENGTH_LONG).show();*/
        }
    }

    @Override
    public void onRoutingCancelled() {
        Toast.makeText(this, "Routing Canceled", Toast.LENGTH_SHORT).show();
    }


    private void changeMapStyle(GoogleMap map) {
        try {
            map.setMapStyle(MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.map_style));


        } catch (Resources.NotFoundException e) {

        }
    }

}
