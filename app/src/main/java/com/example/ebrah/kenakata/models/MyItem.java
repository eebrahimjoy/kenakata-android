package com.example.ebrah.kenakata.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

public class MyItem implements ClusterItem {
    private LatLng latLng;
    private String title;
    private String  snipped;

    public MyItem(LatLng latLng) {
        this.latLng = latLng;
    }

    public MyItem(LatLng latLng, String title, String snipped) {
        this.latLng = latLng;
        this.title = title;
        this.snipped = snipped;
    }

    @Override
    public LatLng getPosition() {
        return latLng;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return snipped;
    }
}
