package com.example.ebrah.kenakata.models.retrofit;

import com.example.ebrah.kenakata.models.Outlet;
import com.example.ebrah.kenakata.models.PlaceAPI.Predictions;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiService {
    @GET("place/autocomplete/json")
    Call<Predictions> getPlacesAutoComplete(
            @Query("input") String input,
            @Query("types") String types,
            @Query("location") String location,
            @Query("radius") String radius,
            @Query("strictbounds") String strictbounds,
            @Query("key") String key
    );

    @GET()
    Call<List<Outlet>> getOutlets(@Url String url);
}
