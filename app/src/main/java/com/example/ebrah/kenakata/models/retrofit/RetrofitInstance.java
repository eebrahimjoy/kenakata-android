package com.example.ebrah.kenakata.models.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {

    private static String BASE_URL_PLACE_API="https://maps.googleapis.com/maps/api/";
    private static Retrofit retrofit;

    private static final String BASE_URL_OUTLET = "http://www.kenakatabd24.somee.com/api/";
    private static Retrofit retrofitOutlet;


    public static Retrofit getRetrofitInstanceForPlaceAPI(){

        if (retrofit==null){
            retrofit = new Retrofit.Builder().baseUrl(BASE_URL_PLACE_API).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }

    public static Retrofit getRetrofitInstanceForOutletAPI(){

        if (retrofitOutlet==null){
            retrofitOutlet = new Retrofit.Builder().baseUrl(BASE_URL_OUTLET).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofitOutlet;
    }


}
