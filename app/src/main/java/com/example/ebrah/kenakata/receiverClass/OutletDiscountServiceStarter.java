package com.example.ebrah.kenakata.receiverClass;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.example.ebrah.kenakata.serviceClass.OutletDiscountService;

public class OutletDiscountServiceStarter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Service is below
        Intent serviceIntent = new Intent(context.getApplicationContext(), OutletDiscountService.class);
        context.startService(serviceIntent);
    }
}
