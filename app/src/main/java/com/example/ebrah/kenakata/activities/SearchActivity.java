package com.example.ebrah.kenakata.activities;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ebrah.kenakata.R;
import com.example.ebrah.kenakata.adapters.PlacesAutoCompleteAdapter;
import com.example.ebrah.kenakata.databinding.ActivitySearchBinding;
import com.example.ebrah.kenakata.models.PlaceAPI.Prediction;
import com.example.ebrah.kenakata.models.PredictionInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SearchActivity extends AppCompatActivity implements PredictionInterface {
    private PlacesAutoCompleteAdapter placesAutoCompleteAdapter;
    private ActivitySearchBinding binding;
    private List<Prediction> predictions;
    private Geocoder geocoder;
    private double latFromAutoSearch;
    private double lonFromAutoSearch;
    private String addressLine;
    public static TextView noLacationText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        noLacationText = findViewById(R.id.noLoacationFoundTV);
        initRecyclerView();
        hideSearchViewIcon();
        searchViewAction();
        binding.clickedBackBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });



    }
    public static void update_counter(String value,int i){
        try
        {
            if (i ==1){
                noLacationText.setVisibility(View.VISIBLE);
                noLacationText.setText(value);
            }
            if (i == 0){
                noLacationText.setVisibility(View.INVISIBLE);
            }
        }
        catch (Exception e){

        }
    }


    private void searchViewAction() {

        binding.searchlocationSV.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    predictions.clear();
                    placesAutoCompleteAdapter.notifyDataSetChanged();
                }
                placesAutoCompleteAdapter.getFilter().filter(newText);
                return true;
            }
        });

    }

    private void hideSearchViewIcon() {
        int magId = getResources().getIdentifier("android:id/search_mag_icon", null, null);
        ImageView magImage = (ImageView) binding.searchlocationSV.findViewById(magId);
        magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
        magImage.setVisibility(View.GONE);
    }

    private void initRecyclerView() {
        predictions = new ArrayList<>();
        placesAutoCompleteAdapter = new PlacesAutoCompleteAdapter(getApplicationContext()
                , predictions, this);
        binding.locationNameRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.locationNameRecyclerView.setAdapter(placesAutoCompleteAdapter);
    }

    @Override
    public void getPrediction(Prediction prediction) {
        geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocationName(prediction.getDescription(), 1);
            hideSoftKeyboard();
            if (addresses.size() > 0) {
                latFromAutoSearch = addresses.get(0).getLatitude();
                lonFromAutoSearch = addresses.get(0).getLongitude();
                addressLine = addresses.get(0).getAddressLine(0);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("latitude", latFromAutoSearch);
                returnIntent.putExtra("longitude", lonFromAutoSearch);
                returnIntent.putExtra("lineAddress", addressLine);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void hideSoftKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }
}